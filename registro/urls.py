from django.urls import path
from . import views

urlpatterns = [
    path('',views.index,name= "index"),
    path('registro/',views.registro,name="registro"),
    path('catalogo/',views.catalogo,name="catalogo"),
    path('contacto/',views.contacto,name="contacto"),
    path('servicios/',views.servicios,name="servicios"),
    path('tecnicos/',views.tecnicos,name="tecnicos")
]