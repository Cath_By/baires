from django.shortcuts import render

def index(request):
    return render(request,'index.html')

def registro(request):
    return render(request,'registro.html')

def catalogo(request):
    return render(request,'catalogo.html')

def contacto(request):
    return render(request,'contacto.html')

def servicios(request):
    return render(request,'servicios.html') 

def tecnicos(request):
    return render(request,'tecnicos.html')