from django.db import models

"""class Comuna(models.Model):
    id_comuna= models.IntegerField(primary_key=True)
    nombre_comuna = models.CharField(max_length=100)
    

class Sucursal(models.Model):
    id_sucursal = models.IntegerField(primary_key=True)
    direccion = models.CharField(max_length=500)
    comuna = models.ForeignKey(Comuna, null=True)
    

class Producto(models.Model):
    id_producto= models.IntegerField(primary_key=True)
    nom_producto = models.CharField(max_length=100)
    precio = models.IntegerField()
    modelo = models.CharField(max_length=200)
    marca = models.CharField(max_length=100)
   
class Vendedor(models.Model):
    rut_vendedor = models.CharField(primary_key=True,max_length=18)
    nombres = models.CharField(max_length=200)
    apellido_pat = models.CharField(max_length=100)
    apellido_mat = models.CharField(max_length=100)
    area = models.CharField(max_length=100)
    correo_empresa = models.EmailField()
    sucursal = models.ForeignKey(Sucursal)
    
class Tecnico(models.Model):
    id_tecnico = models.IntegerField(primary_key=True)
    rut_tecnico = models.CharField(max_length=18)
    nombres_tecnico = models.CharField(max_length=200)
    apellido_pat_tec = models.CharField(max_length=100)
    apellido_mat_tec = models.CharField(max_length=100)
    sucursal = models.ForeignKey(Sucursal)
    area_trabajo = models.ForeignKey(Area_trabajo)
   
class Area_trabajo(models.Model):
    id_area= models.IntegerField(primary_key=True)
    nom_area = models.CharField(max_length=100)
   
class Tipo_cliente(models.Model):
    id_tipo_cliente= models.IntegerField(primary_key=True)
    tipo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=500)
        
class Cliente(models.Model):
    rut_cliente= models.CharField(primary_key=True,max_length=18)
    nombres = models.CharField(max_length=200)  
    apellido_pat_cli =models.CharField(max_length=100)
    apellido_mat_cli = models.CharField(max_length=100)
    tipo_cliente = models.ForeignKey(Tipo_cliente)
      
class Tipo_compra(models.Model):
    id_tipo_compra = models.IntegerField(primary_key=True)
    desc_tipo_compra = models.CharField(max_length=300)
    

class Compra(models.Model):
    id_compra = models.IntegerField(primary_key=True)
    fecha_compra = models.DateField(default=timezone.now)
    hora_compra = models.TimeField()
    total_compra = models.IntegerField()
    cant_products = models.IntegerField()
    tipo_compra = models.ForeignKey(Tipo_compra)
    producto = models.ForeignKey(Producto)
    

class Servicio(models.Model):
    id_servicio = models.IntegerField(primary_key=True)   
    tipo_servicio = models.CharField(max_length=100)
    desc_servicio = models.TextField(max_length=300)  
    valor_servicio = models.IntegerField()
    sucursal = models.ForeignKey(Sucursal) 
    
class Despacho(models.Model):
    id_despacho = models.IntegerField(primary_key=True)   
    fecha_despacho = models.DateField(default=timezone.now)
    hora_despacho = models.TimeField()
    cant_productos = models.IntegerField()
    direccion_despacho = models.CharField(max_length=300)     
    sucursal = models.ForeignKey(Sucursal)
    producto = models.ForeignKey(Producto)
    cliente = models.ForeignKey(Cliente)
   
"""


